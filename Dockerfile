FROM nginx:alpine

COPY ./nginx.conf /etc/nginx/
COPY ./entrypoint.sh /

ENTRYPOINT [ "/bin/sh" ]
CMD [ "/entrypoint.sh" ]