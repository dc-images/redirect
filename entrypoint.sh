#!/bin/sh

val()
{
  VARIABLE=$1
  DEFAULT_VALUE=$2

  if [ -z $VARIABLE ]; then
    echo $DEFAULT_VALUE
  else
    echo $VARIABLE
  fi
}

SERVER_NAME=$(val $SERVER_NAME "_")
SERVER_PORT=$(val $SERVER_PORT "")
SERVER_PORT_SSL=$(val $SERVER_PORT_SSL "")

if [ ! -z "$SERVER_PORT" ]; then
  SERVER_PORT="listen $SERVER_PORT;"
fi

if [ ! -z "$SERVER_PORT_SSL" ]; then
  SERVER_PORT_SSL="listen $SERVER_PORT_SSL;"
fi

REDIRECT_ENABLE=$(val $REDIRECT_ENABLE "0")
REDIRECT_TO=$(val $REDIRECT_TO "http://www.example.com")
REDIRECT_METHOD=$(val $REDIRECT_METHOD "redirect")
REDIRECT_EXPRESSION=$(val $REDIRECT_EXPRESSION "^/$")

sed -i -e "
s#%server_name%#$SERVER_NAME#g
s#%server_port%#$SERVER_PORT#g
s#%server_port_ssl%#$SERVER_PORT_SSL#g
s#%redirect_enable%#$REDIRECT_ENABLE#g
s#%redirect_to%#$REDIRECT_TO#g
s#%redirect_method%#$REDIRECT_METHOD#g
s#%redirect_expression%#$REDIRECT_EXPRESSION#g
" /etc/nginx/nginx.conf

# Debug
cat /etc/nginx/nginx.conf

echo "Starting nginx"

exec nginx;